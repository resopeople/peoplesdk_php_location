<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\requisition\request\info\library;

use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\location\config\library\ConstConfig;



class ConstConfigSndInfo
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get array of header keys,
     * per attribute key.
     *
     * @return array
     */
    public static function getTabHeaderKey()
    {
        // Return result
        return array(
            ConstConfig::ATTRIBUTE_KEY_GET_TIMEZONE_NAME => ConstSndInfo::HEADER_KEY_LOC_GET_TIMEZONE_NAME,
            ConstConfig::ATTRIBUTE_KEY_GET_DATETIME_FORMAT => ConstSndInfo::HEADER_KEY_LOC_GET_DATETIME_FORMAT,
            ConstConfig::ATTRIBUTE_KEY_SET_TIMEZONE_NAME => ConstSndInfo::HEADER_KEY_LOC_SET_TIMEZONE_NAME,
            ConstConfig::ATTRIBUTE_KEY_SET_DATETIME_FORMAT => ConstSndInfo::HEADER_KEY_LOC_SET_DATETIME_FORMAT
        );
    }



    /**
     * Get array of URL argument keys,
     * per attribute key.
     *
     * @return array
     */
    public static function getTabUrlArgKey()
    {
        // Return result
        return array(
            ConstConfig::ATTRIBUTE_KEY_GET_TIMEZONE_NAME => ConstSndInfo::URL_ARG_KEY_LOC_GET_TIMEZONE_NAME,
            ConstConfig::ATTRIBUTE_KEY_GET_DATETIME_FORMAT => ConstSndInfo::URL_ARG_KEY_LOC_GET_DATETIME_FORMAT,
            ConstConfig::ATTRIBUTE_KEY_SET_TIMEZONE_NAME => ConstSndInfo::URL_ARG_KEY_LOC_SET_TIMEZONE_NAME,
            ConstConfig::ATTRIBUTE_KEY_SET_DATETIME_FORMAT => ConstSndInfo::URL_ARG_KEY_LOC_SET_DATETIME_FORMAT
        );
    }



}