<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\location\config\model\ConfigEntity;
use people_sdk\location\config\requisition\request\info\library\ConstConfigSndInfo;



class ToolBoxConfigSndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with specified location,
     * from specified location configuration entity.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithParam() return format.
     *
     * @param ConfigEntity $configEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithLocation(
        ConfigEntity $configEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);
        $tabHeaderKey = ConstConfigSndInfo::getTabHeaderKey();
        $tabUrlArgKey = ConstConfigSndInfo::getTabUrlArgKey();
        $result = $tabInfo;

        // Run each attribute
        foreach($configEntity->getTabData() as $strKey => $value)
        {
            // Register attribute value, if required
            if(!is_null($value))
            {
                $result = ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? $tabHeaderKey[$strKey] : null),
                    ((!$boolOnHeaderRequired) ? $tabUrlArgKey[$strKey] : null),
                    $result
                );
            }
        }

        // Return result
        return $result;
    }



}