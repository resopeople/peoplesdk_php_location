<?php
/**
 * This class allows to define location configuration entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use people_sdk\location\config\library\ConstConfig;



/**
 * @property null|string strAttrGetTzName
 * @property null|string strAttrGetDtFormat
 * @property null|string strAttrSetTzName
 * @property null|string|array attrSetDtFormat
 */
class ConfigEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute get timezone name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstConfig::ATTRIBUTE_KEY_GET_TIMEZONE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstConfig::ATTRIBUTE_ALIAS_GET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstConfig::ATTRIBUTE_NAME_SAVE_GET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute get datetime format
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstConfig::ATTRIBUTE_KEY_GET_DATETIME_FORMAT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstConfig::ATTRIBUTE_ALIAS_GET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstConfig::ATTRIBUTE_NAME_SAVE_GET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute set timezone name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstConfig::ATTRIBUTE_KEY_SET_TIMEZONE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstConfig::ATTRIBUTE_ALIAS_SET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstConfig::ATTRIBUTE_NAME_SAVE_SET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute set datetime format
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstConfig::ATTRIBUTE_KEY_SET_DATETIME_FORMAT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstConfig::ATTRIBUTE_ALIAS_SET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstConfig::ATTRIBUTE_NAME_SAVE_SET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidString = function($boolNullRequire = true)
        {
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty']
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a string, not empty'
                    ]
                ]
            );

            if($boolNullRequire)
            {
                $result[0][1]['rule_config']['is-null'] = array('is_null');
                $result[0][1]['error_message_pattern'] = '%1$s must be null or a string, not empty';
            }

            return $result;
        };

        // Init var
        $result = array(
            ConstConfig::ATTRIBUTE_KEY_GET_TIMEZONE_NAME => $getTabRuleConfigValidString(),
            ConstConfig::ATTRIBUTE_KEY_GET_DATETIME_FORMAT => $getTabRuleConfigValidString(),
            ConstConfig::ATTRIBUTE_KEY_SET_TIMEZONE_NAME => $getTabRuleConfigValidString(),
            ConstConfig::ATTRIBUTE_KEY_SET_DATETIME_FORMAT => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => $getTabRuleConfigValidString(false),
                            'is-valid-iterate-string' => [
                                ['type_array'],
                                [
                                    'sub_rule_iterate',
                                    [
                                        'rule_config' => $getTabRuleConfigValidString(false)
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string/array of strings, not empty'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstConfig::ATTRIBUTE_KEY_GET_TIMEZONE_NAME:
            case ConstConfig::ATTRIBUTE_KEY_GET_DATETIME_FORMAT:
            case ConstConfig::ATTRIBUTE_KEY_SET_TIMEZONE_NAME:
            case ConstConfig::ATTRIBUTE_KEY_SET_DATETIME_FORMAT:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}