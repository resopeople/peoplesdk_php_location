<?php
/**
 * This class allows to define location configuration entity factory class.
 * Location configuration entity factory allows to provide new location configuration entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use people_sdk\location\config\model\ConfigEntity;



/**
 * @method ConfigEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class ConfigEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ConfigEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new ConfigEntity(
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}