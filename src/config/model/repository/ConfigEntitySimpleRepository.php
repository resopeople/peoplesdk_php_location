<?php
/**
 * This class allows to define location configuration entity simple repository class.
 * Location configuration entity simple repository is simple repository,
 * allows to prepare loaded data, from requisition persistence, for location configuration entity.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\model\repository;

use people_sdk\library\model\repository\simple\model\SimpleRepository;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\requester\api\RequesterInterface;
use liberty_code\requisition\requester\client\library\ConstClientRequester;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\requisition\response\library\ToolBoxResponse;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\location\config\library\ConstConfig;
use people_sdk\location\config\model\ConfigEntity;



class ConfigEntitySimpleRepository extends SimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Requester instance.
     * @var RequesterInterface
     */
    protected $objRequester;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequesterInterface $objRequester
     */
    public function __construct(
        RequesterInterface $objRequester,
        DefaultPersistor $objPersistor = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init properties
        $this->objRequester = $objRequester;

        // Call parent constructor
        parent::__construct(
            $objPersistor,
            $objRequestSndInfoFactory
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                ]
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ]
                ]
            ]
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ConfigEntity::class,
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for standard sub-action
                ConstConfig::SUB_ACTION_TYPE_STANDARD => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/location/config'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH => ConstResponse::BODY_KEY_RESULT
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstConfig::SUB_ACTION_TYPE_STANDARD
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of timezone names,
     * available for location configuration.
     * Response can be returned, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see RequesterInterface::executeRequestConfig() configuration array format.
     *
     * @param null|array $tabConfig = null
     * @param null|DataHttpResponse &$objResponse = null
     * @return null|array
     */
    public function getTabTzName(
        array $tabConfig = null,
        DataHttpResponse &$objResponse = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = array(
            ConstClientRequester::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_DATA,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ]
                ]
            ]
        );
        $tabConfig = (
            (!is_null($tabExecConfig)) ?
                ToolBoxTable::getTabMerge($tabExecConfig, $tabConfig):
                $tabConfig
        );
        $objRequestSndInfoFactory = $this->getObjRequestSndInfoFactory();
        /** @var null|DataHttpResponse $objResponse */
        $objResponse = (
            (!is_null($objRequestSndInfoFactory)) ?
                $this
                    ->objRequester
                    ->executeRequestConfig(
                        array(
                            ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP,
                            ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => $objRequestSndInfoFactory->getTabSndInfo(
                                array(
                                    ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/location/config/timezone'
                                )
                            )
                        ),
                        $tabConfig
                    ) :
                null
        );
        $result = (
            (
                (!is_null($objResponse)) &&
                (!is_null($tabResultData = ToolBoxResponse::getTabResultData($objResponse))) &&
                is_array($tabResultData)
            ) ?
                array_filter(
                    array_values($tabResultData),
                    function($data) {return is_string($data);}
                ) :
                null
        );

        // Return result
        return $result;
    }



}


