<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\location\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_GET_TIMEZONE_NAME = 'strAttrGetTzName';
    const ATTRIBUTE_KEY_GET_DATETIME_FORMAT = 'strAttrGetDtFormat';
    const ATTRIBUTE_KEY_SET_TIMEZONE_NAME = 'strAttrSetTzName';
    const ATTRIBUTE_KEY_SET_DATETIME_FORMAT = 'attrSetDtFormat';

    const ATTRIBUTE_ALIAS_GET_TIMEZONE_NAME = 'dt-config-get-tz-name';
    const ATTRIBUTE_ALIAS_GET_DATETIME_FORMAT = 'dt-config-get-dt-format';
    const ATTRIBUTE_ALIAS_SET_TIMEZONE_NAME = 'dt-config-set-tz-name';
    const ATTRIBUTE_ALIAS_SET_DATETIME_FORMAT = 'dt-config-set-dt-format';

    const ATTRIBUTE_NAME_SAVE_GET_TIMEZONE_NAME = 'dt-config-get-tz-name';
    const ATTRIBUTE_NAME_SAVE_GET_DATETIME_FORMAT = 'dt-config-get-dt-format';
    const ATTRIBUTE_NAME_SAVE_SET_TIMEZONE_NAME = 'dt-config-set-tz-name';
    const ATTRIBUTE_NAME_SAVE_SET_DATETIME_FORMAT = 'dt-config-set-dt-format';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_STANDARD = 'standard';



}